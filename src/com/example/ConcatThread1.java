package com.example;

class ConcatThread1 extends Thread{
    Thread thread;
    Concatenation concatination;

    public ConcatThread1(Concatenation concatination) {
        this.concatination = concatination;
    }
    public void run() {
        concatination.conCat("Hii", "hello");
    }
}
class ConcatThread2 extends Thread {
    Thread thread;
    Concatenation concatenation;

    public ConcatThread2(Concatenation concatenation) {
        this.concatenation = concatenation;
    }
    public void run() {
        concatenation.conCat("hii", "friends");
    }
}
class ConcatThread3 extends Thread {
    Thread thread;
    Concatenation concatenation;

    public ConcatThread3(Concatenation concatenation) {
        this.concatenation = concatenation;
    }
    public void run() {

        concatenation.conCat("good", "morning");
    }
}
