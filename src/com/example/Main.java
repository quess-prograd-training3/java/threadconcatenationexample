package com.example;

public class Main {
    public static void main(String[] args) {
        Concatenation concatenation=new Concatenation();
        ConcatThread1 concatThread1=new ConcatThread1(concatenation);
        ConcatThread2 concatThread2=new ConcatThread2(concatenation);
        ConcatThread3 concatThread3=new ConcatThread3(concatenation);
        concatThread1.start();
        concatThread2.start();
        concatThread3.start();
    }
}
