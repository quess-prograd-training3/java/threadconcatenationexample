package example2;

public class Concatenation2 extends Thread{
    private String  s1;
    private String  s2;

    @Override
    public void run() {
        synchronized (this) {
            System.out.println(s1 + s2);
        }
    }

    Thread thread;
    public Concatenation2(String s1, String s2,String threadName) {
        this.s1 = s1;
        this.s2 = s2;
        thread=new Thread(this,threadName);
        thread.start();
    }
}
