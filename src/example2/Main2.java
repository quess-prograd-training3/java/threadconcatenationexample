package example2;

public class Main2 {
    public static void main(String[] args) {
        Concatenation2 concatenation1 = new Concatenation2("Hii", "hello", "thread 1");
        concatenation1.setPriority(10);
        Concatenation2 concatenation2 = new Concatenation2("hii", "friends", "thread 2");
        concatenation2.setPriority(9);
        Concatenation2 concatenation3 = new Concatenation2("good", "morning", "thread 3");
        concatenation3.setPriority(8);
    }
}
